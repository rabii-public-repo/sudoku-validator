package com.sudoku;

import java.util.HashSet;
import java.util.Set;

public class Solution {

	static boolean checkIfSubGridExistInGrid(Integer[][] subGrid, Integer[][] grid) {
		int count;
		for (int i = 0; i < grid.length; i = i + 3) {
			for (int j = 0; j < grid.length; j = j + 3) {
				count = 0;
				for (int k = 0; k < 3; k++) {
					for (int m = 0; m < 3; m++) {
						if (grid[i][j] == subGrid[k][m]) {
							count++;
						}
					}
				}
				if (count == 9) {
					return true;
				}
			}
		}
		return false;
	}

	static boolean isGridValid(Integer[][] grid) {

		// check if subGrid is unique in the grid
		for (int i = 0; i < grid.length; i = i + 3) {
			for (int l = 0; l < grid.length; l = l + 3) {
				Integer[][] subGrid = new Integer[3][3];
				for (int j = 0; j < subGrid.length; j++) {
					for (int k = 0; k < subGrid.length; k++) {
						subGrid[j][k] = grid[j][k];
					}
				}
				if (checkIfSubGridExistInGrid(subGrid, grid)) {
					return false;
				}
			}
		}

		// check if a sub grid is correct
		for (int i = 0; i < grid.length; i = i + 3) {
			Set<Integer> cellSet = new HashSet<Integer>();
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					cellSet.add(grid[j][k]);
				}
			}
			if (cellSet.size() != grid.length) {
				return false;
			}
		}
		// check if line is correct
		for (int i = 0; i < grid.length; i++) {
			Set<Integer> lineSet = new HashSet<Integer>();
			for (int j = 0; j < grid.length; j++) {
				lineSet.add(grid[i][j]);
			}
			if (lineSet.size() != grid.length) {
				return false;
			}
		}
		// check if column is correct
		for (int i = 0; i < grid.length; i++) {
			Set<Integer> columnSet = new HashSet<Integer>();
			for (int j = 0; j < grid.length; j++) {
				columnSet.add(grid[j][i]);
			}
			if (columnSet.size() != grid.length) {
				return false;
			}
		}
		return true;
	}
}

